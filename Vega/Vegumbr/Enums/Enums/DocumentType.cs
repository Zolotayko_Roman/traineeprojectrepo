﻿using System.ComponentModel;

public enum DocumentType
{

    [Description("comments")]
    Comments = 0,
    [Description("blogPost")]
    Blog = 1,
    [Description("articles")]
    Article = 2

}
