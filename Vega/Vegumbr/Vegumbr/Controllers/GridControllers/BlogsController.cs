﻿using Lecoati.LeBlender.Extension.Controllers;
using Lecoati.LeBlender.Extension.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Umbraco.Web;
using Vegumbr.Models;
using Umbraco.Core.Models;

namespace Vegumbr.Controllers.GridControllers
{
    public class BlogsController : LeBlenderController
    {
        public ActionResult Index(LeBlenderModel model)
        {
            LeBlenderValue properties = model.Items.First();


            var title = properties.GetValue<string>("blockTitle");
            var contentNodes = properties.GetValue<ICollection<IPublishedContent>>("multiNode");
            var buttonTitle = properties.GetValue<string>("buttonTitle");
            var buttonLink = properties.GetValue<string>("buttonLink");
            string _currentUserName = "";

            if (CustomRoleProvider.GetCurrentUser().ToArray().Count() != 0)
            {
                _currentUserName = CustomRoleProvider.GetCurrentUser().First().ToString();
            }

            var viewModel = new BlockGridModel()
            {
                Title = title,
                Content = contentNodes.Select(ToGridModel).ToList(),
                ButtonLink = buttonLink,
                ButtonName = buttonTitle,
                CurrentUserName = _currentUserName,
            };

            return View(viewModel);
        }

        private GridModel ToGridModel(IPublishedContent content)
        {
            return new GridModel
            {
                Author = content.GetPropertyValue<string>("author") ?? content.CreatorName,
                Topic = content.GetPropertyValue<string>("topic"),
                Description = StripHTML(content.GetPropertyValue<string>("description")),
                Photo = content.GetPropertyValue<IPublishedContent>("mediaPhoto"),
                Tags = content.GetPropertyValue<string[]>("tags"),
                Url = content.Url()
            };
        }

        private string StripHTML(string htmlString)
        {
            return Regex.Replace(htmlString, "<.*?>", String.Empty);
        }
    }
}