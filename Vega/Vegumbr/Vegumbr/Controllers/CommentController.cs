﻿using System;
using System.Linq;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;
using Umbraco.Core.Security;
using Umbraco.Core.Services;
using Umbraco.Web.Mvc;
using Vega.BLL.Contracts.Services;
using Vegumbr.Models;
using Umbraco.Core;

namespace Vegumbr.Controllers
{
    public class CommentController : SurfaceController
    {
        public const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/";

        private readonly ICommentService commentService;

        private readonly int CurrentMemberId = umbraco.cms.businesslogic.member.Member.CurrentMemberId();
        private readonly string CurrentUserName = CustomRoleProvider.GetCurrentUser().FirstOrDefault().ToString()??null;

        public CommentController(ICommentService _commentService)
        {
            commentService = _commentService;
        }

        [HttpPost]
        public ActionResult Submit(CommentViewModel model)
        {
            //var ticket = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current).GetUmbracoAuthTicket();
            //if (ticket != null)
            //{
            //    //var user = Umbraco.Core.ApplicationContext.Current.Services.UserService.GetByUsername(ticket.Name);
            //}
            //var member = Services.MemberService.GetByUsername("qq@mail.com");

            model.Author = CurrentUserName;
            model.UmbracoMemberId = CurrentMemberId;
            commentService.Create(Mapper.Mapper.ToDto(model));
            return PartialView(PARTIAL_VIEW_FOLDER + "_SuccessAlert.cshtml");
        }

        public ActionResult GetCommentsByPost(int id)
        {
            var publishedItems = commentService.GetPublishedByPost(id).Select(Mapper.Mapper.ToEntity);
            var CurrentUserName1 = CustomRoleProvider.GetCurrentUser().First().ToString();
            return PartialView(PARTIAL_VIEW_FOLDER + "_CommentsListPartial.cshtml", publishedItems);
        }

        public ActionResult Delete(Guid id, int parentId, int umbracoId)
        {
            IContentService service = ApplicationContext.Services.ContentService;

            IContent comment = service.GetById(umbracoId);
            service.Delete(comment);
            commentService.Remove(id);

            return RedirectToUmbracoPage(parentId);

        }

    }
}