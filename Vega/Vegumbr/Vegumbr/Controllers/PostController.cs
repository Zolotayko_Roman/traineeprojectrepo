﻿using System;
using System.Linq;
using System.Web.Mvc;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Vegumbr.Models;
using Vegumbr.Models.Posts;

namespace Vegumbr.Controllers
{
    public class PostController : SurfaceController
    {
        public const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/";

        [HttpGet]
        public ActionResult NewPostForm()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Submit(PostViewModel model)
        {
            CreateNewPost(model);

            return PartialView(PARTIAL_VIEW_FOLDER + "_SuccessPostFromUI.cshtml");
        }

        private void CreateNewPost(PostViewModel model)
        {
            var userName = CustomRoleProvider.GetCurrentUser().First().ToString();
            IContentService contentService = Services.ContentService;
            var publishDate = DateTime.Now;
            var content = contentService.CreateContent("PostFromUI", 1119, "blog");

            content.ReleaseDate = publishDate;
            content.CreateDate = publishDate;
            content.UpdateDate = publishDate;
            content.SetValue("author", userName);
            content.SetValue("description", model.Description);
            content.SetValue("topic", model.Topic);

            contentService.Save(content);
        }
    }
}