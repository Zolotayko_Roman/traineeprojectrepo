﻿using Vega.BLL.Models;
using Vegumbr.Models;

namespace Vegumbr.Controllers.Mapper
{
    public class Mapper
    {
        public static CommentModel ToDto(CommentViewModel model)
        {
            return new CommentModel
            {
                Author = model.Author,
                Message = model.Message,
                CreateDate = model.CreateDate,
                DocumentType = model.DocumentType,
                Id = model.Id,
                PostId = model.PostId,
                Published = model.Published,
                UmbracoId = model.UmbracoId,
                UmbracoMemberId = model.UmbracoMemberId
            };
        }

        public static CommentViewModel ToEntity(CommentModel model)
        {
            return new CommentViewModel
            {
                Author = model.Author,
                Message = model.Message,
                CreateDate = model.CreateDate,
                DocumentType = model.DocumentType,
                Id = model.Id,
                PostId = model.PostId,
                Published = model.Published,
                UmbracoId = model.UmbracoId,
                UmbracoMemberId = model.UmbracoMemberId
            };
        }
    }
}