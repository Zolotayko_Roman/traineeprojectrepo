﻿using System.Net.Http.Formatting;
using umbraco.BusinessLogic.Actions;
using Umbraco.Core;
using Umbraco.Core.Models.Membership;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;
using Constants = Umbraco.Core.Constants;

namespace Vegumbr.Controllers.ApproveIt
{
    [PluginController("approveIt")]
    [Tree("approveIt", "approveItTree", "Content to publish")]
    public class ApproveItTreeController : TreeController
    {
        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            var nodes = new TreeNodeCollection();

            IUser user = UmbracoContext.Security.CurrentUser;

            if (id == Constants.System.Root.ToInvariantString())
            {
                TreeNode node = CreateTreeNode("comments", "-1", queryStrings, "Comments", "icon-umb-content", true);
                nodes.Add(node);
            }
            else
            {
                if (id == "comments")
                {
                    TreeNode nodeBlog = CreateTreeNode("blogPost", "comments", queryStrings, "Blogs", "icon-umb-content");
                    TreeNode nodeArticle = CreateTreeNode("articles", "comments", queryStrings, "Articles", "icon-umb-content");
                    nodes.Add(nodeBlog);
                    nodes.Add(nodeArticle);
                }
            }
            return nodes;
        }

        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            var menu = new MenuItemCollection();

            if (id == Constants.System.Root.ToInvariantString())
            {
                menu.Items.Add<RefreshNode, ActionRefresh>(ApplicationContext.Services.TextService.Localize(ActionRefresh.Instance.Alias));
            }
            return menu;
        }
    }
}
