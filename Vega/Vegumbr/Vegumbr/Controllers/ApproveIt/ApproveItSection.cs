﻿using umbraco.businesslogic;
using umbraco.interfaces;

namespace Vegumbr.Controllers.ApproveIt
{
    [Application("approveIt", "qwerty", "icon-wand", 15)]
    public class ApproveItSection : IApplication { }
}