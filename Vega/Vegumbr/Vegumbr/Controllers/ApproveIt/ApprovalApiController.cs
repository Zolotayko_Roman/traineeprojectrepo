﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Umbraco.Core.Services;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;
using Vega.BLL.Contracts.Services;
using Vegumbr.Models;

namespace Vegumbr.Controllers.ApproveIt
{
    [PluginController("approveIt")]
    public class ApprovalApiController : UmbracoAuthorizedJsonController
    {
        private readonly ICommentService commentService = DependencyResolver.Current.GetService<ICommentService>();

        public JsonResult GetById(string id)
        {
            if (id == GetEnumDescription(DocumentType.Blog))
            {
                return ToJson(commentService.GetUnpublished((int)DocumentType.Blog).OrderByDescending(x => x.CreateDate).Select(Mapper.Mapper.ToEntity).ToList());
            }
            if (id == GetEnumDescription(DocumentType.Article))
            {
                return ToJson(commentService.GetUnpublished((int)DocumentType.Article).OrderByDescending(x => x.CreateDate).Select(Mapper.Mapper.ToEntity).ToList());
            }
            if (id == GetEnumDescription(DocumentType.Comments))
            {
                return ToJson(commentService.GetUnpublished((int)DocumentType.Comments).OrderByDescending(x => x.CreateDate).Select(Mapper.Mapper.ToEntity).ToList());
            }
            return new JsonResult();
        }

        public CommentViewModel PostPublish(CommentViewModel model)
        {
            model.UmbracoId = CreateComment(model.Author, model.Message, model.PostId);
            return Mapper.Mapper.ToEntity(commentService.UpdateState(model.Id, model.UmbracoId));
        }

        public void Remove(string id)
        {
            commentService.Remove(Guid.Parse(id));
        }

        public void Edit(CommentViewModel model)
        {
            commentService.Update(Mapper.Mapper.ToDto(model));
        }

        public JsonResult GetPublishedItems(string id)
        {
            if (id == GetEnumDescription(DocumentType.Blog))
            {
                return ToJson(commentService.GetPublished((int)DocumentType.Blog).OrderByDescending(x => x.CreateDate).Select(Mapper.Mapper.ToEntity).ToList());
            }

            if (id == GetEnumDescription(DocumentType.Article))
            {
                return ToJson(commentService.GetPublished((int)DocumentType.Article).OrderByDescending(x => x.CreateDate).Select(Mapper.Mapper.ToEntity).ToList());
            }
            if (id == GetEnumDescription(DocumentType.Comments))
            {
                return ToJson(commentService.GetPublished((int)DocumentType.Comments).OrderByDescending(x => x.CreateDate).Select(Mapper.Mapper.ToEntity).ToList());
            }
            return new JsonResult();
        }


        private JsonResult ToJson(IEnumerable<CommentViewModel> commentsList)
        {
            JsonResult jsonData = new JsonResult
            {
                Data = commentsList
            };
            return jsonData;
        }

        private int CreateComment(string commentAuthor, string commmentMessage, int parentId)
        {
            int userId = 0;
            IContentService contentService = Services.ContentService;
            var content = contentService.CreateContent(commentAuthor, parentId, "comment", userId);
            content.SetValue("commentAuthor", commentAuthor);
            content.SetValue("commentMessage", commmentMessage);
            content.SetValue("parentId", parentId);
            contentService.SaveAndPublishWithStatus(content);
            return content.Id;
        }

        private string GetEnumDescription(Enum value)
        {
            FieldInfo field = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return value.ToString();
            }
        }
    }
}
