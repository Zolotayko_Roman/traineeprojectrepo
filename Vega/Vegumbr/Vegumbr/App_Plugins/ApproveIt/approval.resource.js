﻿angular.module("umbraco.resources")
    .factory("approvalResource", function ($http) {
        return {
            getById: function (id) {
                return $http.get("backoffice/ApproveIt/ApprovalApi/GetById?id=" + id);
            },
            publish: function (field) {
                return $http.post("backoffice/ApproveIt/ApprovalApi/PostPublish?id=",field);
            },
            remove: function (id) {
                console.log(id)
                return $http.post("backoffice/ApproveIt/ApprovalApi/Remove?id=" + id);
            },
            edit: function (field) {
                return $http.post("backoffice/ApproveIt/ApprovalApi/edit", field);
            },
            getPublishedItems: function (id) {
                return $http.get("backoffice/ApproveIt/ApprovalApi/getPublishedItems?id=" + id);
            }

        };
    });