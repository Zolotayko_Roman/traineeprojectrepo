﻿angular.module("umbraco").controller("Approval.ApprovalEditController",
    function ($scope, $routeParams, approvalResource, notificationsService, navigationService, userService) {

        $scope.comments = []

        $scope.publishedComments = []

        $scope.content = {
            tabs: [
                { id: 1, label: "Unpublished" },
                { id: 2, label: "Published" }
            ]
        };

        $scope.sortColumn = "Date";
        $scope.reverseSort = false;

        $scope.loaded = false;
        userService.getCurrentUser().then(function (currentUser) {
            console.log('getCurrentUser', currentUser)
            $scope.user = currentUser;
        });

        if ($routeParams.id == -1) {
            $scope.node = {};
            $scope.loaded = true;
        }
        else {
            approvalResource.getById($routeParams.id).then(function (response) {
                console.log('getById', response)
                $scope.comments = response.data;
                $scope.loaded = true;
                console.log($scope.comments)
            });
        }

        $scope.publish = function (node) {
            console.log('publish', node)
            approvalResource.publish(node).then(function (response) {
                $scope.node = response.data;
                $scope.contentForm.$dirty = false;
                navigationService.syncTree({ tree: 'approveItTree', path: [-1, -1], forceReload: true });
                $scope.index = $scope.comments.Data.indexOf(node);
                $scope.comments.Data.splice($scope.index, 1);
                $scope.publishedComments.Data.push(node);
                notificationsService.success("Success", node.Author + " has been published");
            });
        };

        $scope.saveField = function (field) {
            if ($scope.editing !== false) {
                $scope.comments[$scope.editing] = $scope.newField;
                $scope.editing = false;
                approvalResource.edit(field);
                notificationsService.success("Success item has been saved");
            }
        };

        $scope.cancel = function () {
            if ($scope.editing !== false) {
                $scope.comments[$scope.editing] = $scope.newField;
                $scope.editing = false;
            }
        };

        $scope.remove = function (node) {
            approvalResource.remove(node.Id);
            $scope.index = $scope.comments.Data.indexOf(node);
            $scope.comments.Data.splice($scope.index, 1);
            notificationsService.success("Success", node.Author + " has been removed");
        }

        $scope.sortData = function (column) {
            $scope.reverseSort = ($scope.sortColumn == column) ? !$scope.reverseSort : false;
            $scope.sortColumn = column;
        }



        $scope.getPublishedItems = function () {
            approvalResource.getPublishedItems($routeParams.id).then(function (response) {
                console.log('getPublishedItems', response.data)
                $scope.publishedComments = response.data;
            })
        }
    });