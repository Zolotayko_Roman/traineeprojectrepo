﻿using IdentityModel;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using Umbraco.Web.Security.Providers;

namespace Vegumbr.Models
{
    public class CustomRoleProvider : MembersRoleProvider
    {

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new AccessViolationException("This Provider is Read-Only");
        }

        public override void CreateRole(string roleName)
        {
            throw new AccessViolationException("This Provider is Read-Only");
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new AccessViolationException("This Provider is Read-Only");
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new AccessViolationException("This Provider is Read-Only");
        }

        public override string[] GetAllRoles()
        {
            return new[] { "Standard User" };
        }

        public override string[] GetRolesForUser(string username)
        {
            var userIdentity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;

            var roles = userIdentity.Claims.Where(c => c.Type.Equals(JwtClaimTypes.Role)).Select(c => c.Value).ToArray();

            var currentUser = userIdentity.Claims.Where(c => c.Type.Equals(JwtClaimTypes.Name)).Select(c => c.Value).ToArray();

            return roles;
        }


        public static string[] GetCurrentUser()
        {
            string[] undefinedUser = {"Guest"};

            var userIdentity = (ClaimsIdentity)Thread.CurrentPrincipal.Identity;
            var currentUser = userIdentity.Claims.Where(c => c.Type.Equals(JwtClaimTypes.Name)).Select(c => c.Value).ToArray();

            if (currentUser.Count() == 0)
            {
                currentUser = undefinedUser;
            }
            
            return currentUser;

        }

        public override bool IsUserInRole(string username, string roleName)
        {
            string[] userRoles = GetRolesForUser(username);
            return userRoles.Contains(roleName);
        }

        public override bool RoleExists(string roleName)
        {

            return GetAllRoles().Contains(roleName);
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

    }
}