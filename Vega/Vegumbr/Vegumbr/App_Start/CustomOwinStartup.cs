﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using Umbraco.Web;
using Umbraco.Web.Security.Identity;
using Vegumbr.App_Start;
using Vegumbr;
using Umbraco.Core.Security;

[assembly: OwinStartup("CustomOwinStartup", typeof(CustomOwinStartup))]

namespace Vegumbr.App_Start
{
    public class CustomOwinStartup : UmbracoDefaultOwinStartup
    {

        protected override void ConfigureServices(IAppBuilder app)
        {
            base.ConfigureServices(app);

        }


        protected override void ConfigureMiddleware(IAppBuilder app)
        {

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                Provider = new CookieAuthenticationProvider()
            });

            JwtSecurityTokenHandler.InboundClaimTypeMap = new Dictionary<string, string>();

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            app.UseIdentityServerAuthentication("http://localhost:5000", "umbraco", "http://localhost:5001/");


            app.UseUmbracoPreviewAuthentication(ApplicationContext, PipelineStage.Authorize);
            app.ConfigureUserManagerForUmbracoBackOffice(
              Umbraco.Core.ApplicationContext.Current,

              global::Umbraco.Core.Security.MembershipProviderExtensions.GetUsersMembershipProvider().AsUmbracoMembershipProvider());


            app
                .UseUmbracoBackOfficeCookieAuthentication(Umbraco.Core.ApplicationContext.Current)
                .UseUmbracoBackOfficeExternalCookieAuthentication(Umbraco.Core.ApplicationContext.Current);

        }

    }
}