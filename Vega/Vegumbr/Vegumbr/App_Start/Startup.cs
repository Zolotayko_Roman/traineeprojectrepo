﻿using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Vega.IoC.Infrastructure;
using Vegumbr.Models;

namespace Vegumbr.Controllers
{
    public class Startup : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            NinjectModule serviceModulde = new ServiceModule();
            NinjectModule controllerModule = new ControllerModule();

            var kernel = new StandardKernel(serviceModulde, controllerModule);
            kernel.Unbind<ModelValidatorProvider>();
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }
    }
}