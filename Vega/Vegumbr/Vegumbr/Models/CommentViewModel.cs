﻿using System;

namespace Vegumbr.Models
{
    public class CommentViewModel 
    {
        public Guid Id { get; set; }

        public string Author { get; set; }

        public string Message { get; set; }

        public int PostId { get; set; }

        public bool Published { get; set; }

        public DateTime CreateDate { get; set; }

        public int UmbracoId { get; set; }

        public int DocumentType { get; set; }

        public int UmbracoMemberId { get; set; }
    }
}