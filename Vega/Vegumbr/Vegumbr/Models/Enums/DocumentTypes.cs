﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Vegumbr.Models
{
    public enum DocumentType
    {
        
        [Description("comments")]
        Comments=0,
        [Description("blogPost")]
        Blog,
        [Description("articles")]
        Article

    }
}