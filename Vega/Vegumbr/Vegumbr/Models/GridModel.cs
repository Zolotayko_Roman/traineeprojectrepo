﻿using Lecoati.LeBlender.Extension.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace Vegumbr.Models
{
    public class GridModel : LeBlenderModel
    {
        public string Author { get; set; }

        public string Topic { get; set; }

        public string Description { get; set; }

        public IPublishedContent Photo { get; set; }

        public string[] Tags { get; set; }

        public string Url { get; set; }

        public string Title { get; set; }

        public string CurrentUserName { get; set; }


    }
}