﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace Vegumbr.Models
{
    public class BlockGridModel
    {
        public List<GridModel> Content { get; set; }

        public string ButtonName { get; set; }

        public string Title { get; set; }

        public string ButtonLink { get; set; }

        public string CurrentUserName { get; set; }
    }
}