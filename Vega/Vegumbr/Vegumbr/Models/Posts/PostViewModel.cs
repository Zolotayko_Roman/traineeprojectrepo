﻿using System.Collections.Generic;

namespace Vegumbr.Models.Posts
{
    public class PostViewModel
    {
        public string PageTitle { get; set; }

        public string NavigationTitle { get; set; }

        public string Author { get; set; }

        public string ButtonPicker { get; set; }

        public string Description { get; set; }

        public IEnumerable<string> Tags { get; set; }

        public string Topic { get; set; }
    }
}