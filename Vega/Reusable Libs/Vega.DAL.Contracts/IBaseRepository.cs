﻿using System;
using Vega.DTO.DAL_BLL.Interfaces;

namespace Vega.DAL.Contracts
{
    public interface IBaseRepository<TDto>
    {
        Guid Create(TDto dto);

        Guid Remove(Guid id);

        IDataComment Update(TDto dto);
    }
}
