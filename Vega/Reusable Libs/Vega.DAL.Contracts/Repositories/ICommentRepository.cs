﻿using System;
using System.Collections.Generic;
using Vega.DTO.DAL_BLL.Interfaces;

namespace Vega.DAL.Contracts.Repositories
{
    public interface ICommentRepository : IBaseRepository<IDataComment>
    {
        IEnumerable<IDataComment> GetPublished(int documentType);

        IEnumerable<IDataComment> GetUnpublished(int documentType);

        IEnumerable<IDataComment> GetPublishedByPost(int id);

        IDataComment UpdateState(Guid id, int umbracoId);
    }
}
