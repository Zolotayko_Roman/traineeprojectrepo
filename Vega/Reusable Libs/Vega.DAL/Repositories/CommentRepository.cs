﻿using System;
using System.Collections.Generic;
using Vega.DAL.Contracts.Repositories;
using Vega.DAL.Entities;
using Vega.DAL.Extensions;
using Vega.DTO.DAL_BLL.Interfaces;

namespace Vega.DAL.Repositories
{
    public class CommentRepository : BaseRepository<Comment, IDataComment>, ICommentRepository
    {
        public override Guid Create(IDataComment dto)
        {
            return base.Create(dto);
        }

        public override Guid Remove(Guid id)
        {
            return base.Remove(id);
        }

        public override IDataComment Update(IDataComment dto)
        {
            return base.Update(dto);
        }

        public override IDataComment UpdateState(Guid id, int umbracoId)
        {
            return base.UpdateState(id, umbracoId);
        }

        public override IEnumerable<IDataComment> GetPublishedByPost(int id)
        {
            return base.GetPublishedByPost(id);
        }

        public override IEnumerable<IDataComment> GetPublished(int documentType)
        {
            return base.GetPublished(documentType);
        }

        public override IEnumerable<IDataComment> GetUnpublished(int documentType)
        {
            return base.GetUnpublished(documentType);
        }

        public override Comment ToEntity(IDataComment dto)
        {
            return dto.ToEntity();
        }

        public override IDataComment ToDto(Comment entity)
        {
            return entity.ToDto();
        }
    }
}
