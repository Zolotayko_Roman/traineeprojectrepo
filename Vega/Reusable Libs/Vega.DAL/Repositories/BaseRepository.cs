﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Vega.DAL.EF;
using Vega.DAL.Entities;
using Vega.DTO.DAL_BLL.Interfaces;

namespace Vega.DAL.Repositories
{
    public abstract class BaseRepository<TEntity, TDto> where TEntity : BaseEntity where TDto : IDataInterface
    {

        public virtual Guid Create(TDto dto)
        {
            return ExecuteConnectionWithUsing((context) =>
            {
                TEntity entity = ToEntity(dto);
                entity.CreateDate = DateTime.UtcNow;
                context.Entry(entity).State = EntityState.Added;
                context.SaveChanges();
                return entity.Id;
            });
        }

        public virtual TDto Update(TDto dto)
        {
            return ExecuteConnectionWithUsing((context) =>
            {
                TEntity entity = ToEntity(dto);
                entity.CreateDate = dto.CreateDate;
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
                return ToDto(entity);
            });
        }

        public virtual Guid Remove(Guid id)
        {
            return ExecuteConnectionWithUsing((context) =>
            {
                var selectedEntity = context.Set<TEntity>().Find(id);
                context.Set<TEntity>().Remove(selectedEntity);
                context.SaveChanges();
                return selectedEntity.Id;
            });
        }

        public virtual IEnumerable<TDto> GetPublished(int documentType)
        {
            return ExecuteConnectionWithUsing((context) =>
            {
                if (documentType == 0)
                {
                    return context.Set<TEntity>().Where(item => item.Published).Select(ToDto).ToList();
                }
                else if (documentType == 1)
                {
                    return context.Set<TEntity>().Where(item => item.DocumentType == documentType && item.Published).Select(ToDto).ToList();
                }
                else
                {
                    return context.Set<TEntity>().Where(item => item.DocumentType == documentType && item.Published).Select(ToDto).ToList();
                }
            });
        }

        public virtual IEnumerable<TDto> GetUnpublished(int documentType)
        {
            return ExecuteConnectionWithUsing((context) =>
            {
                if (documentType == 0)
                {
                    return context.Set<TEntity>().Where(item => !item.Published).Select(ToDto).ToList();
                }
                else if (documentType == 1)
                {
                    return context.Set<TEntity>().Where(item => item.DocumentType == documentType && !item.Published).Select(ToDto).ToList();
                }
                else
                {
                    return context.Set<TEntity>().Where(item => item.DocumentType == documentType && !item.Published).Select(ToDto).ToList();
                }
            });
        }

        public virtual IEnumerable<TDto> GetPublishedByPost(int id)
        {
            return ExecuteConnectionWithUsing((context) =>
            {
                return context.Set<TEntity>().Where(item => item.PostId == id && item.Published).Select(ToDto).ToList();
            });
        }

        public virtual TDto UpdateState(Guid id, int umbracoId)
        {
            return ExecuteConnectionWithUsing((context) =>
            {
                var entity = context.Set<TEntity>().Find(id);
                if (entity.Published)
                {
                    entity.Published = false;
                }
                else
                {
                    entity.Published = true;
                }
                entity.UmbracoId = umbracoId;
                context.SaveChanges();
                return ToDto(entity);
            });
        }

        public abstract TDto ToDto(TEntity entity);

        public abstract TEntity ToEntity(TDto dto);

        protected T ExecuteConnectionWithUsing<T>(Func<BlogContext, T> func)
        {
            using (BlogContext contex = new BlogContext())
            {
                return func(contex);
            }
        }
    }
}
