﻿namespace Vega.DAL.Entities
{
    public class Comment : BaseEntity
    {
        public string Author { get; set; }

        public string Message { get; set; }

        public int UmbracoMemberId { get; set; }
    }
}
