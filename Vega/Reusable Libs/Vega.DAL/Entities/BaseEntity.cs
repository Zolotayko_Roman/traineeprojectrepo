﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vega.DAL.Entities
{

    public class BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public DateTime CreateDate { get; set; }

        public bool Published { get; set; }

        public int DocumentType { get; set; }

        public int PostId { get; set; }

        public int UmbracoId { get; set; }
    }
}
