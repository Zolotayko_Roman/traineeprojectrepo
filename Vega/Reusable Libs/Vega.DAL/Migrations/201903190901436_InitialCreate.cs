namespace Vega.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Author = c.String(),
                        Message = c.String(),
                        UmbracoMemberId = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        Published = c.Boolean(nullable: false),
                        DocumentType = c.Int(nullable: false),
                        PostId = c.Int(nullable: false),
                        UmbracoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Comments");
        }
    }
}
