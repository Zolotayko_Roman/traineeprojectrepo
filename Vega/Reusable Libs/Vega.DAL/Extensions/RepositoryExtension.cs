﻿using Vega.DAL.Entities;
using Vega.DTO.DAL_BLL.DTO_s;
using Vega.DTO.DAL_BLL.Interfaces;

namespace Vega.DAL.Extensions
{
    public static class PostRepositoryExtension
    {
        public static Comment ToEntity(this IDataComment model)
        {
            return new Comment
            {
                Author = model.Author,
                Message = model.Message,
                CreateDate = model.CreateDate,
                DocumentType = model.DocumentType,
                Id = model.Id,
                PostId = model.PostId,
                Published = model.Published,
                UmbracoId = model.UmbracoId,
                UmbracoMemberId = model.UmbracoMemberId
            };
        }

        public static CommentDto ToDto(this Comment model)
        {
            return new CommentDto
            {
                Author = model.Author,
                Message = model.Message,
                CreateDate = model.CreateDate,
                DocumentType = model.DocumentType,
                Id = model.Id,
                PostId = model.PostId,
                Published = model.Published,
                UmbracoId = model.UmbracoId,
                UmbracoMemberId = model.UmbracoMemberId
            };
        }
    }
}
