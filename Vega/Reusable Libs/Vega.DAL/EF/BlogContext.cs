﻿using System;
using System.Data.Entity;
using Vega.DAL.Entities;

namespace Vega.DAL.EF
{
    public class BlogContext : DbContext, IDisposable
    {
        public BlogContext() : base("VegaDB") { }

        public DbSet<Comment> Comments { get; set; }
    }
}
