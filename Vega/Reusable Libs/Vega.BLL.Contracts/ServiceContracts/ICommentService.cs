﻿using System;
using System.Collections.Generic;
using Vega.DTO.DAL_BLL.DTO_s;

namespace Vega.BLL.Contracts.Services
{
    public interface ICommentService:IBaseService<CommentModel>
    {
        IEnumerable<CommentModel> GetCommentByPostId(Guid id);

    }
}
