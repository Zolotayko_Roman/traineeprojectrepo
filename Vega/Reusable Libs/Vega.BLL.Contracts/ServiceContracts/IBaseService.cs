﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vega.BLL.Contracts.Services
{
    public interface IBaseService<TDto>
    {
        Guid Create(TDto dto);

        void Remove(Guid id);

        void Update(TDto dto);

        IEnumerable<TDto> GetAll();
    }
}
