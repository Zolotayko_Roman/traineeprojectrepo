﻿namespace Vega.DTO.DAL_BLL.Interfaces
{
    public interface IDataComment : IDataInterface
    {
        string Author { get; }

        string Message { get; }

        int PostId { get; }

        int UmbracoId { get; }

        int DocumentType { get; }

        int UmbracoMemberId { get; }
    }
}
