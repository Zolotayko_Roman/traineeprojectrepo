﻿using System;

namespace Vega.DTO.DAL_BLL.Interfaces
{
    public interface IDataInterface
    {
        Guid Id { get; }

        DateTime CreateDate { get; }

        bool Published { get; }
    }
}
