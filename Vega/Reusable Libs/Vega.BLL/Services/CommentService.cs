﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vega.BLL.Contracts.Services;
using Vega.BLL.Models;
using Vega.DAL.Contracts.Repositories;

namespace Vega.BLL.Services
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository baseRepository;

        private readonly Mapper mapper;

        public CommentService(ICommentRepository _baseRepository, Mapper _mapper)
        {
            baseRepository = _baseRepository;
            mapper = _mapper;
        }
        public Guid Create(CommentModel commentModel)
        {
            return baseRepository.Create(Mapper.ToDto(commentModel));
        }

        public void Remove(Guid id)
        {
            baseRepository.Remove(id);
        }

        public CommentModel Update(CommentModel commentModel)
        {
            return Mapper.ToEntity(baseRepository.Update(Mapper.ToDto(commentModel)));
        }

        public CommentModel UpdateState(Guid id, int umbracoId)
        {
            return Mapper.ToEntity(baseRepository.UpdateState(id, umbracoId));
        }

        public IEnumerable<CommentModel> GetPublished(int documentType)
        {
            return baseRepository.GetPublished(documentType).Select(Mapper.ToEntity).ToList();
        }

        public IEnumerable<CommentModel> GetUnpublished(int documentType)
        {
            return baseRepository.GetUnpublished(documentType).Select(Mapper.ToEntity).ToList();
        }

        public IEnumerable<CommentModel> GetPublishedByPost(int id)
        {
            return baseRepository.GetPublishedByPost(id).Select(Mapper.ToEntity).ToList();
        }
    }
}
