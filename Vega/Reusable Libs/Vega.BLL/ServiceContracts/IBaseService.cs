﻿using System;
using Vega.BLL.Models;

namespace Vega.BLL.Contracts.Services
{
    public interface IBaseService<TDto>
    {
        Guid Create(TDto dto);

        void Remove(Guid id);

        CommentModel Update(TDto dto);
    }
}
