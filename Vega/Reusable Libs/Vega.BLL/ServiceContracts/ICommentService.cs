﻿using System;
using System.Collections.Generic;
using Vega.BLL.Models;

namespace Vega.BLL.Contracts.Services
{
    public interface ICommentService : IBaseService<CommentModel>
    {
        IEnumerable<CommentModel> GetPublished(int documentType);

        IEnumerable<CommentModel> GetUnpublished(int documentType);

        IEnumerable<CommentModel> GetPublishedByPost(int id);

        CommentModel UpdateState(Guid id, int umbracoId);
    }
}
