﻿using Vega.BLL.Models;
using Vega.DTO.DAL_BLL.DTO_s;
using Vega.DTO.DAL_BLL.Interfaces;

namespace Vega.BLL
{
    public class Mapper
    {
        public static IDataComment ToDto(CommentModel model)
        {
            return new CommentDto
            {
                Author = model.Author,
                Message = model.Message,
                CreateDate = model.CreateDate,
                DocumentType = model.DocumentType,
                Id = model.Id,
                PostId = model.PostId,
                Published = model.Published,
                UmbracoId = model.UmbracoId,
                UmbracoMemberId = model.UmbracoMemberId
            };
        }

        public static CommentModel ToEntity(IDataComment model)
        {
            return new CommentModel
            {
                Author = model.Author,
                Message = model.Message,
                CreateDate = model.CreateDate,
                DocumentType = model.DocumentType,
                Id = model.Id,
                PostId = model.PostId,
                Published = model.Published,
                UmbracoId = model.UmbracoId,
                UmbracoMemberId = model.UmbracoMemberId
            };
        }
    }
}
