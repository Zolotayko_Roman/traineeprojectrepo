﻿using System;

namespace Vega.DTO.DAL_BLL.Interfaces
{
    public interface ICommentModel
    {
        Guid Id { get; }

        string Author { get; }

        string Message { get; }

        int PostId { get; }

        bool Published { get; }

        DateTime CreateDate { get; }

        int UmbracoId { get; }

        int DocumentType { get; }

        int UmbracoMemberId { get; }
    }
}
