﻿using Ninject.Modules;
using Vega.BLL.Contracts.Services;
using Vega.BLL.Services;

namespace Vega.IoC.Infrastructure
{
    public class ControllerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ICommentService>().To<CommentService>();
        }
    }
}
