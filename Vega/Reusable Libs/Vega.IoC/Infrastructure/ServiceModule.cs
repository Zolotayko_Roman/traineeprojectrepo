﻿using Ninject.Modules;
using Vega.DAL.Contracts.Repositories;
using Vega.DAL.Repositories;

namespace Vega.IoC.Infrastructure
{
    public class ServiceModule : NinjectModule
    {

        public override void Load()
        {
            Bind<ICommentRepository>().To<CommentRepository>();
        }
    }
}
